const { DataTypes } = require("sequelize");
const { db } = require("../config");

const User = db.define(
  "user",
  {
    id: {
      type: DataTypes.NUMBER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    email: {
      type: DataTypes.STRING,
      field: "email",
      validate: {
        is: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
      },
    },
    firstName: {
      type: DataTypes.STRING,
      field: "firstName",
    },
    lastName: {
      type: DataTypes.STRING,
      field: "lastName",
    },
    location: {
      type: DataTypes.STRING,
      field: "location",
      validate: {
        is: /.+[A-Za-z]\/.+[A-Za-z]/,
      },
    },
    birthdayDate: {
      type: DataTypes.DATEONLY,
      field: "birthdayDate",
    },
    hasEmailSent: {
      type: DataTypes.BOOLEAN,
      field: "hasEmailSent",
    },
  },
  {
    tableName: "user",
  }
);

module.exports = User;
