const express = require("express");
const app = express();
const cors = require("cors");
const cron = require("node-cron")
const { db, PORT } = require("./config");

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());

cron.schedule("0 0 */1 * * *", require("./controllers/user/birthdayEmail"));

app.get("/", (req, res) => {
  res.send("Welcome to Digital Envision API");
});
app.use("/user", require("./controllers/user/route"));

app.get("*", (req, res) => {
  res.send("404 Page Not Found");
});

if (db) {
  app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
  });
}
