const { QueryTypes } = require("sequelize");
const { db } = require("../../config");
const User = require("../../models/user");
const moment = require("moment");
const fetch = (...args) =>
  import("node-fetch").then(({ default: fetch }) => fetch(...args));

const sentEmail = async (req, res) => {
  try {
    const currentTime = moment();
    const users = await db.query(
      "select * FROM `user` WHERE DAY(birthdayDate) = DAY(CURDATE()) AND MONTH(birthdayDate) = MONTH(CURDATE());",
      {
        type: QueryTypes.SELECT,
      }
    );

    users.forEach((user) => {
      const userTimeHour = currentTime.tz(user.location).format("hh");
      const userTimeMinutes = currentTime.tz(user.location).format("mm");

      if (
        Number(userTimeHour) === 9 &&
        Number(userTimeMinutes) >= 0 &&
        user.hasEmailSent !== true
      ) {
        const options = {
          method: "POST",
          body: JSON.stringify({
            email: user.email,
            message: `Hey,${user.firstName} ${user.lastName} it’s your birthday.`,
          }),
          headers: { "Content-Type": "application/json" },
        };

        fetch(
          "https://email-service.digitalenvision.com.au/send-email",
          options
        )
          .then(() => {
            User.update(
              {
                hasEmailSent: true,
              },
              {
                where: {
                  email: user.email,
                },
              }
            );
          })
          .catch((error) => {
            console.log(error);
          });
      } else {
        User.update(
          {
            hasEmailSent: false,
          },
          {
            where: {
              email: user.email,
            },
          }
        );
      }
    });

    return res.send({ message: "Email sent" });
  } catch (error) {
    return res
      .status(502)
      .send({ message: "Something error", detailMessage: error });
  }
};

module.exports = sentEmail;
