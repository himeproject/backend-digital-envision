const router = require("express").Router();

router.post("/", require("./create"));
router.delete("/", require("./delete"));
router.put("/", require("./update"));

module.exports = router;
