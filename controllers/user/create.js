const User = require("../../models/user");
const moment = require("moment");

const create = async (req, res) => {
  try {
    const listOfZones = moment.tz.names();
    const findEmail = await User.findOne({
      where: {
        email: req.body.email,
      },
    });

    if (findEmail) {
      return res.status(403).send({ message: "User has been registered" });
    }

    if (listOfZones.includes(req.body.location)) {
      const result = await User.create({
        ...req.body,
      });

      return res.send({ message: "User has been created", data: result });
    } else {
      return res.status(403).send({ message: "Location has not on the list" });
    }
  } catch (error) {
    return res
      .status(502)
      .send({ message: "Something error", detailMessage: error });
  }
};

module.exports = create;
