const User = require("../../models/user");

const create = async (req, res) => {
  try {
    const findEmail = await User.findOne({
      where: {
        email: req.body.email,
      },
    });
    if (findEmail) {
      await User.destroy({
        where: {
          email: req.body.email,
        },
      });

      return res.send({ message: "User has been removed" });
    } else {
      return res.send({ message: "User not found" });
    }
  } catch (error) {
    return res
      .status(502)
      .send({ message: "Something error", detailMessage: error });
  }
};

module.exports = create;
