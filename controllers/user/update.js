const User = require("../../models/user");
const moment = require("moment");

const update = async (req, res) => {
  try {
    const listOfZones = moment.tz.names();
    const findEmail = await User.findOne({
      where: {
        email: req.body.email,
      },
    });

    for (let key in req.body) {
      if (req.body[key] === "") {
        delete req.body[key];
      }
    }

    const checkingLocation = () => {
      if (!req.body.location) return;
      if (req.body.location && listOfZones.includes(req.body.location)) {
        return req.body.location;
      } else {
        return res
          .status(403)
          .send({ message: "Location has not on the list" });
      }
    };

    const { email, ...rest } = req.body;
    const result = await User.update(
      {
        ...rest,
        hasEmailSent: 0,
        location: checkingLocation(),
      },
      {
        where: {
          email,
        },
      }
    );

    return res.send({ message: "updated completed", data: result });
  } catch (error) {
    console.log(error);
    return res
      .status(502)
      .send({ message: "Something error", detailMessage: error });
  }
};

module.exports = update;
