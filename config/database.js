const { Sequelize } = require("sequelize");
const {
  MYSQL_DATABASE,
  MYSQL_HOST,
  MYSQL_USER,
  MYSQL_PASSWORD,
} = require("./environment");

const db = new Sequelize(MYSQL_DATABASE, MYSQL_USER, MYSQL_PASSWORD, {
  host: MYSQL_HOST,
  dialect: "mysql",
  define: {
    timestamps: false,
  },
  logging: false,
});

db.authenticate()
  .then(() => {
    console.log(`Connected to Database`);
  })
  .catch((error) => {
    console.log(error);
    return res.status(502).send({ message: 'Terjadi Kesalahan', detailMessage: error });
  });

module.exports = db;
