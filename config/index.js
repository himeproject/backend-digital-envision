const db = require("./database");
const { PORT } = require("./environment");

module.exports = {
  PORT,
  db,
};
