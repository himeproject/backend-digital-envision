# How to Install

- Make sure you have to install nodejs on your local machine
- Then restore this db on your local machine
- Then fill the env file with yours
- Run `yarn install`
- then run `yarn node`
- make sure there is no issue when you start this app
- the last program run

# How to use

This app have 3 route `create`, `delete` and `update`

## How to Create method `POST` -> route `/user`

- open postman with url `YOUR_LOCALHOST/user`
- then input on body with `firstName`, `lastName`, `email`, `location` and `birthdayDate`
- if there is no error new user has been created

*Note: please fill location refer from moment timezone name, and birthdayDate eg:1994/7/7*

## How to Delete method `DELETE` -> route `/user`

- open postman with url `YOUR_LOCALHOST/user`
- then input the body with registered `email`
- then user has been removed

## How to PUT method `PUT` -> route `/user`

- open postman with url `YOUR_LOCALHOST/user`
- then input on body with `firstName`, `lastName`, `email`, `location` and `birthdayDate`
- if there is no error new user has been updated

*Note: make sure the user has been registered before*